#!/usr/bin/env python
#  $Id: tktest.py,v 1.1 2009/11/19 23:44:46 dave Exp $
#  -----------------------------------------------------------------
#
#  NAME:     tktest.py
#
#  PURPOSE:  Test interaction when combining Tkinter and PyGist
#
#  NOTES:
#  Tkinter and PyGist do not work together. 
#  Problem:  Tkinter does not have a proper idle event.
#  Responds to Tk events and Gist mouse events, but:
#  - Gist window does not update properly 
#  - Python command line input is no longer accepted
#
#  -----------------------------------------------------------------

import tkinter
import sys
from . import tkgist
from .gist import *
from . import gistdemolow

def DrawOval(Event):
    # Event.widget will be the main canvas:
    Event.widget.create_oval(Event.x-5,Event.y-5, Event.x+5,Event.y+5)
def DrawRectangle(Event):
    Event.widget.create_rectangle(Event.x-5,Event.y-5, Event.x+5,Event.y+5)
def MoveButton(Side):
    # The methods pack_forget() and grid_forget() unpack
    # a widget, but (unlike the destroy() method)
    # do not destroy it; it can be re-displayed later.
    QuitButton.pack_forget()
    QuitButton.pack(side=Side)

def plot():
   print(".. Gist button pressed")
   plg([0,1])

def main():
   root=tkinter.Tk()
   MainCanvas=tkinter.Canvas(root)
   MainCanvas.bind("<Button-1>",DrawOval)
   MainCanvas.bind("<Button-3>",DrawRectangle)
   MainCanvas.pack(fill=tkinter.BOTH,expand=tkinter.YES)

   gistButton=tkinter.Button(MainCanvas,text="Gist",
    command=plot)
   quitButton=tkinter.Button(MainCanvas,text="Quit",
    command=sys.exit)
   gistButton.pack(side=tkinter.TOP)
   quitButton.pack(side=tkinter.BOTTOM)

   root.bind("<Up>",lambda e:MoveButton(tkinter.TOP))
   root.bind("<Down>",lambda e:MoveButton(tkinter.BOTTOM))
   root.bind("<Left>",lambda e:MoveButton(tkinter.LEFT))
   root.bind("<Right>",lambda e:MoveButton(tkinter.RIGHT))
   root.geometry("300x300") # Set minimum window size

   root.mainloop()

if (__name__=="__main__"):
   main()
